import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { UserLdap } from 'src/model/user-ldap';
import { UsersService } from '../service/users.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-ldap-detail',
  templateUrl: './ldap-detail.component.html',
  styleUrls: ['./ldap-detail.component.scss']
})
export class LdapDetailComponent implements OnInit {

  user: UserLdap;
  processLoadRunning = false;
  processValidateRunning = false;

  userForm = this.fb.group ({
    login: [''], // Valeur de départ vide
    nom: [''],
    prenom: [''],
    // Groupe de données imrbiqué
    passwordGroup: this.fb.group({
      password: [''],
      confirmPassword: ['']
    }),
    mail: { value: '', disabled: true},
  })
  constructor(
    private usersService: UsersService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ){ }

  ngOnInit(): void {
    this.getUser();
  }

  private getUser(): void {
    const login = this.route.snapshot.paramMap.get('id');

    this.usersService.getUser(login).subscribe(
      user => { this.user = user; }
    );
  }

  private formGetValue(name: string ): any {
    return this.userForm.get(name).value;
  }

  public goToLdap(): void {
    this.router.navigate(['/users/list']);
  }

  onSubmitForm(): void{
    // Validation des données (à voir plus tard)
  }

  updateLogin(): void{
    this.userForm.get('login').setValue((this.formGetValue('prenom')
    + '.' + this.formGetValue('nom')).toLowerCase());
    this.updateMail();
  }

  updateMail(): void{
    this.userForm.get('mail').setValue(this.formGetValue('login').toLowerCase()
    + '@epsi.lan');
  }

  isFormMail()  : void{ }
  isFormValid() : boolean { return false; }
}
